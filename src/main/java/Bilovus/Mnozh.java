package Bilovus;

import java.util.Scanner;

public class Mnozh {

    public static int getMnozh(int num)throws MnozhExeption{
        System.out.println("Введіть перший множник:");
        Scanner sc = new Scanner(System.in);
        num = sc.nextInt();
        Scanner sc2 = new Scanner(System.in);
        int y;
        System.out.println("Введіть другий множник");
        y = sc2.nextInt();
        int res;
        if(num==0 | y==0)throw new MnozhExeption("Ви ввели число 0 ",num);

        res = y*num;
        return res;
    }
}
